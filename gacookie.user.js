// ==UserScript==
// @name         GA Cookie
// @namespace    https://www.rubber4roofs.co.uk/
// @version      0.4
// @description  Drop cookie for blocking GA logging
// @author       Christopher Diaper
// @match        https://www.rubber4roofs.co.uk/*
// @match        https://www.roofdepot.co.uk/*
// @match        https://www.resitrix.com/gb/*
// @match        https://shop.resitrix.com/gb/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    window['ga-disable-UA-76349331-1'] = true; // www.roofdepot.co.uk
    window['ga-disable-UA-21348808-1'] = true; // www.rubber4roofs.co.uk
    window['ga-disable-UA-43284552-1'] = true; // www.resitrix.com
    window['ga-disable-UA-43284552-2'] = true; // shop.resitrix.com

})();